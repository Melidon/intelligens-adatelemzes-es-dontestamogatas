import sys
import pandas as pd
from sklearn.metrics import r2_score

PREDICTION_PATH = "./example_submission.csv"
LABELS_PATH = "./example_labels.csv"

df_pred = pd.read_csv(PREDICTION_PATH)
df_labels = pd.read_csv(LABELS_PATH)

assert all(df_pred.index == df_labels.index)

merged_df = pd.merge(df_pred, df_labels, left_index=True, right_index=True, how='inner')
print("R2 score", r2_score(merged_df.y, merged_df.pred))