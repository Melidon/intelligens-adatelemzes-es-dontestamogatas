# A megoldás rövid menete

## Adatok felosztása tanuló és teszt adathalmazra

A feladat nem specifikálta, hogy miképp kell a felosztani az adatokat, több ötletem is volt.
Végül azt választottam, hogy az értékeléseket random megkeverem, az első 80%-át használom tanításra, a maradékot pedig tesztelésre.

## Predikciós ajánlások

Itt sem volt specifikálva, hogy milyen módszert használjak, úgyhogy kitaláltam egyet ami úgy nagyjából jónak tűnik.

Az eljárásom a következő lépésekből áll:
- Veszek egy felhasználót
- A modellel predikciót készítek az összes filmre
- Azoknál a filmeknél, amiket a felhasználó már értékelt, a predikciót nullára állítom
- Rendezem a predikciókat értékelés szerint csökkenő sorrendbe
- Az első 10-et ajánlom a felhasználónak

Ezt az eljárást minden felhasználóra végrehajtom.

# A teszthalmazon elért eredmény

Itt is csak kitaláltam valamit ami úgy értelmesen hangzik.

A tesztelésre félretett adatokból a következőképp állítottam elő az elvárt eredményeket:
- Fogtam egy felhasználót
- Azokat a filmeket számítom elvártan ajánlottnak, amikre nagyobb mit 3.5 értékelést adott.

Ezt az eljárást is minden felhasználóra végrehajtom.

Ezután minden felhasználóra kiszámolom a precision, recall és f1 értékeket, majd ezek átlagát veszem.
Itt mindegyik érték 0.01 körül alakult, ami nagyon rossz, de azért legalább nem 0.

A konklúzióm, hogy a megadott metrikák teljesen bele vannak erőszakolva a feladatba, sokkal több értelmét látnám, ha mondjuk azt kéne nézni, hogy a modell milyen értéket tippel egy konkrét felhasználó és film párosra, és ezeket összehasonlítani a tesztelésre félretett adatokkal.
